START TRANSACTION;

ALTER TABLE "AbpAuditLogs" ADD "CustID" character varying(100) NULL;

ALTER TABLE "AbpAuditLogs" ADD "E-Message-ID" character varying(200) NULL;

ALTER TABLE "AbpAuditLogs" ADD "T24CustomerID" character varying(100) NULL;

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20240218124536_AddLogContributorFields', '6.0.5');

COMMIT;

